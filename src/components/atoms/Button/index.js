import React from 'react'

import './Button.scss';
export default function Button({onClick, title, loading}) {
  if(loading){
    return <button className="btn-primary disabled">Loading...</button>
  }
  return (
    <button onClick={onClick} className="btn-primary">{title}</button>
  )
}
