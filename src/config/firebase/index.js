import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

var firebaseConfig = {
  apiKey: "AIzaSyBliTZgLyIGYxO_nsyQz95BeFzVrzneKlM",
  authDomain: "atomic-design.firebaseapp.com",
  databaseURL: "https://atomic-design.firebaseio.com",
  projectId: "atomic-design",
  storageBucket: "atomic-design.appspot.com",
  messagingSenderId: "252292854464",
  appId: "1:252292854464:web:f28a2eaf5cdf36114bb325"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
export const database = firebase.database();

export default firebase;