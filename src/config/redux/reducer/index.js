import { CHANGE_ISLOGIN, CHANGE_USER, CHANGE_LOADING, CHANGE_PROMO } from '../action/actionTypes';

const initialState = {
	popup: false,
	isLogin: false,
	isLoading: false,
	user: {},
	promo: [],
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case CHANGE_ISLOGIN:
			return {
				...state,
				isLogin: action.value,
			};
		case CHANGE_USER:
			return {
				...state,
				user: action.value,
			};
		case CHANGE_LOADING:
			return {
				...state,
				isLoading: action.value,
			};
		case CHANGE_PROMO:
			return {
				...state,
				promo: action.value,
			};
		default:
			return state;
	}
};

export default reducer;
