import firebase, { database } from '../../firebase';
import { CHANGE_ISLOGIN, CHANGE_USER, CHANGE_LOADING, CHANGE_PROMO } from './actionTypes'

export const registerUserAPi = data => dispatch => {
	return new Promise((resolve, reject) => {
		dispatch({ type: CHANGE_LOADING, value: true });
		firebase
			.auth()
			.createUserWithEmailAndPassword(data.email, data.password)
			.then(res => {
				dispatch({ type: CHANGE_LOADING, value: false });
        console.log('berhasil: ', res);
        resolve(true);
			})
			.catch(function(error) {
				// Handle Errors here.
				dispatch({ type: CHANGE_LOADING, value: false });
				var errorCode = error.code;
				var errorMessage = error.message;
				console.log({ errorCode, errorMessage });
				reject(false)
			});
	});
};

export const loginUserAPi = data => dispatch => {
	return new Promise((resolve, reject) => {
		dispatch({ type: CHANGE_LOADING, value: true });
		firebase
			.auth()
			.signInWithEmailAndPassword(data.email, data.password)
			.then(res => {
				console.log('berhasil: ', res);
				const dataUser = {
					email: res.user.email,
					uid: res.user.uid,
					emailVerified: res.user.emailVerified,
				};
				dispatch({ type: CHANGE_LOADING, value: false });
				dispatch({ type: CHANGE_ISLOGIN, value: true });
				dispatch({ type: CHANGE_USER, value: dataUser });
				resolve(dataUser);
			})
			.catch(function(error) {
				// Handle Errors here.
				dispatch({ type: CHANGE_LOADING, value: false });
				var errorCode = error.code;
				var errorMessage = error.message;
				console.log({ errorCode, errorMessage });
				reject(false);
			});
	});
};

export const addPromoApi = data => dispatch => {
	return new Promise((resolve, reject)=>{
		database.ref('promo/' + data.userId).push({
			title: data.title,
			desc: data.desc,
			price: data.price,
			date: data.date
		}).then(()=>{
			resolve(true)
		})
		.catch((err)=> reject(false))
	})
}

export const updatePromoApi = data => dispatch => {
	return new Promise((resolve, reject)=>{
		database.ref(`promo/${data.userId}/${data.promoId}`).set({
			title: data.title,
			desc: data.desc,
			price: data.price,
			date: data.date
		}).then(()=>{
			resolve(true)
		})
		.catch((err)=> reject(false))
	})
}

export const deletePromoApi = data => dispatch => {
	// console.log('data cek : ', data)
	database.ref(`promo/${data.userId}/${data.promoId}`).remove()
}

export const getPromoApi = userId => dispatch => {
	const urlPromo = database.ref('promo/' + userId);
	urlPromo.on('value', function(snapshot) {
		const data = [];
		snapshot.val() && Object.keys(snapshot.val()).map((key)=>{
			data.push({
				id: key,
				data: snapshot.val()[key]
			})
		})
		dispatch({
			type: CHANGE_PROMO,
			value: data
		})
	});
}