import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Login from '../../containers/pages/Login';
import Register from '../../containers/pages/Register';
import Dasboard from '../../containers/pages/Dasboard';
import Promotion from '../../containers/pages/Promotion';

function notFound(){
	return (
		<div>
			PAGE NOT FOUND
		</div>
	)
}

const PrivateRoute = ({ component: Component, ...rest}) => (
  <Route 
    {...rest}
    render={props => 
      localStorage.getItem('userData') ?
      (
        <Component {...props} />
      ):
      (
        <Redirect
          to={{
            pathname: '/login',
            state: { from: props.location }
          }}
        />
      )
    }
  />
)

export default class Routes extends Component {
	render() {
		return (
			<Router>
				<Switch>
					<PrivateRoute path="/" exact={true} component={Dasboard} />
					<Route path="/register" component={Register} />
					<Route path="/login" component={Login} />
					<PrivateRoute path="/promo" component={Promotion} />
					<Route path="*" component={notFound} />
				</Switch>
			</Router>
		);
	}
}
