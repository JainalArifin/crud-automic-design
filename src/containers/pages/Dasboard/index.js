import React, { useState, useEffect } from 'react';
import Layout from '../../organisms/Layout';

const Dasboard = (props) => {
  const [user, setUser] = useState({});

  useEffect(()=>{
    const userData = JSON.parse(localStorage.getItem('userData'))
    setUser(userData)
  }, [])
	return (
		<Layout>
			<div>
				<h1>Hi, selamat datang <span>{user.email}</span></h1>
			</div>
		</Layout>
	);
};

export default Dasboard;
