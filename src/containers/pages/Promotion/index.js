import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addPromoApi, getPromoApi, updatePromoApi, deletePromoApi } from '../../../config/redux/action';
import Layout from '../../organisms/Layout';
import { Trash2, Edit } from 'react-feather';
import moment from 'moment';
import formatMataUang from '../../../utils/formatMataUang';

import './Promotion.scss';
class Promotion extends Component {
	state = {
		title: '',
		desc: '',
		price: 0,
		promoId: '',
		textButton: 'SIMPAN',
	};

	componentDidMount() {
		let userData = JSON.parse(localStorage.getItem('userData'));
		this.props.getPromo(userData.uid);
	}

	inputChange = (e, type) => {
		this.setState({
			[type]: e.target.value,
		});
	};

	handleAddPromo = async e => {
		e.preventDefault();
		const { title, desc, price, textButton, promoId } = this.state;
		let userData = JSON.parse(localStorage.getItem('userData'));
		const dataPromo = {
			title: title,
			desc: desc,
			price: price,
			date: new Date().getTime(),
			userId: userData.uid,
		};
		if (textButton === 'UPDATE') {
			dataPromo.promoId = promoId;
			const res = await this.props.updatePromo(dataPromo);
			if (res) {
				this.setState({
					title: '',
					desc: '',
					price: 0,
					textButton: 'SIMPAN',
				});
			}
		} else {
			const res = await this.props.addPromo(dataPromo);
			if (res) {
				this.setState({
					title: '',
					desc: '',
					price: 0,
				});
			}
		}
	};
	handleUpdate = item => {
		this.setState({
			title: item.data.title,
			desc: item.data.desc,
			price: item.data.price,
			promoId: item.id,
			textButton: 'UPDATE',
		});
	};
	handleCancel = e => {
		e.preventDefault();
		this.setState({
			title: '',
			desc: '',
			price: 0,
			textButton: 'SIMPAN',
		});
	};

	handleDeletePromo = list => {
		let userData = JSON.parse(localStorage.getItem('userData'));
		const data = {
			userId: userData.uid,
			promoId: list.id,
		};
		this.props.deletePromo(data);
	};
	render() {
		const { title, price, desc, textButton } = this.state;
		const { promo } = this.props;
		return (
			<Layout>
				<div>
					<h2>Promo</h2>
					<form>
						<div>
							<label htmlFor="title" className="label-input">
								Title
							</label>
							<input
								type="text"
								name="title"
								id="title"
								value={title}
								onChange={e => this.inputChange(e, 'title')}
							/>
						</div>
						<div>
							<label htmlFor="price" className="label-input">
								Price
							</label>
							<input
								type="number"
								name="price"
								id="price"
								value={price}
								onChange={e => this.inputChange(e, 'price')}
							/>
						</div>
						<div>
							<label htmlFor="desc" className="label-input">
								Descrition
							</label>
							<textarea id="desc" value={desc} onChange={e => this.inputChange(e, 'desc')}></textarea>
						</div>
						{textButton === 'UPDATE' ? <button onClick={this.handleCancel} className="btn-danger">CANCEL</button> : <div />}
						<button className="btn-primary" onClick={this.handleAddPromo}>
							{textButton}
						</button>
					</form>
					<div>
						{promo.length > 0
							? promo.map(list => (
									<div key={list.id} className="card-item">
										<h1>{list.data.title}</h1>
										<p className="date">{moment(list.data.date).subtract(10, 'days').calendar()}</p>
                    <p>{formatMataUang(list.data.price)}</p>
										<p>{list.data.desc}</p>
										<div>
											<Edit
												onClick={() => this.handleUpdate(list)}
												color="green"
												size={30}
											/>
											<Trash2 
                        color="red" 
                        size={30} 
                        onClick={() => this.handleDeletePromo(list)} 
                        />
										</div>
									</div>
							  ))
							: null}
					</div>
				</div>
			</Layout>
		);
	}
}

const mapStateToProps = state => ({
	user: state.user,
	promo: state.promo,
});

const mapDispatchToProps = dispatch => ({
	addPromo: data => dispatch(addPromoApi(data)),
	getPromo: uid => dispatch(getPromoApi(uid)),
	updatePromo: data => dispatch(updatePromoApi(data)),
	deletePromo: data => dispatch(deletePromoApi(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Promotion);
