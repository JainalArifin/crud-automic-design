import React, { Component } from 'react';
import './Register.scss';
import Button from '../../../components/atoms/Button';
import { connect } from 'react-redux';
import { registerUserAPi } from '../../../config/redux/action';
import { Link } from 'react-router-dom';

class Register extends Component {
	state = {
		email: '',
		password: '',
	};

	handleChangeText = e => {
		this.setState({
			[e.target.id]: e.target.value,
		});
	};

	handleChangeSubmit = async (e) => {
		e.preventDefault();
		const { email, password } = this.state;
		const res = await this.props.registerAPI({ email, password });
		if(res){
			this.setState({ email:'', password: '' })
			this.props.history.push('/login');
		}else{
			console.log('register failed')
		}
	};

	render() {
		const { email, password } = this.state;
		return (
			<div className="card-form">
				<h2>Register</h2>
				<form>
					<div>
						<label htmlFor="email">email</label>
						<input 
							type="email" 
							name="email" 
							id="email" 
							onChange={this.handleChangeText} 
							value={email}
							/>
					</div>
					<div>
						<label htmlFor="password">password</label>
						<input 
							type="password" 
							name="password" 
							id="password" 
							onChange={this.handleChangeText} 
							value={password}
							/>
					</div>
					<div className="btn-form">
						<Link to="/login">
							<button className="btn-secondary">login</button>
						</Link>
						<Button onClick={this.handleChangeSubmit} title="Register" loading={this.props.isLoading} />
					</div>
				</form>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	isLoading: state.isLoading,
});

const mapDispatchToProps = dispatch => ({
	registerAPI: data => dispatch(registerUserAPi(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
