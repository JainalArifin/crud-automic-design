import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loginUserAPi } from '../../../config/redux/action';
import Button from '../../../components/atoms/Button';
import { Link } from 'react-router-dom';

import './Login.scss';
class Login extends Component {
	state = {
		email: '',
		password: '',
	};

	handleChangeText = e => {
		this.setState({
			[e.target.id]: e.target.value,
		});
	};

	handleLogin = async (e) => {
		e.preventDefault();
		const { email, password } = this.state;
		const res = await this.props.loginAPI({ email, password });
		if(res){
			localStorage.setItem('userData', JSON.stringify(res));
			this.setState({ email: '', password: '' });
			this.props.history.push('/');
		}else{
			console.log('login failed')
		}
	};
	render() {
		const { email, password } = this.state;
		return (
			<div className="card-form">
				<h2>Login</h2>
				<form>
					<div>
						<label htmlFor="email">email</label>
						<input 
							type="email" 
							name="email" 
							id="email" 
							placeholder="Eg. ajis@gmail.com"
							onChange={this.handleChangeText} 
							value={email}
							/>
					</div>
					<div>
						<label htmlFor="password">password</label>
						<input 
							type="password" 
							name="password" 
							id="password" 
							onChange={this.handleChangeText} 
							value={password}
							/>
					</div>
					<div className="btn-form">
						<Link to="/register">
							<button className="btn-secondary">register</button>
						</Link>
						<Button 
							onClick={this.handleLogin} 
							title="Login" 
							loading={this.props.isLoading} />
					</div>
				</form>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	isLoading: state.isLoading,
});

const mapDispatchToProps = dispatch => ({
	loginAPI: data => dispatch(loginUserAPi(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
