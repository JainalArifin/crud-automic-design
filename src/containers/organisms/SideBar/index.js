import React, { useState } from 'react';
import './Sidebar.scss';
import { LogOut } from 'react-feather';
import { Redirect, NavLink } from 'react-router-dom';

const SideBar = props => {
	const [redirect, setRedirect] = useState(false);
	function handleExit() {
		localStorage.removeItem('userData');
		setRedirect(true);
	}
	if (redirect) {
		return <Redirect to="/login" />;
	}
	return (
		<div className="sidebar">
			<div>
        <div className="logo">
          zainal-shop
        </div>
				<NavLink 
          activeClassName="active" 
          className="navbar__link" 
          to="/"
          exact
          >
					<div>Dhasboard</div>
				</NavLink>
				<NavLink 
          activeClassName="active" 
          className="navbar__link" 
          to="/promo"
          >
					<div>Promo</div>
				</NavLink>
			</div>
			<div className="close" onClick={() => handleExit()}>
				<LogOut color="white" size={20} />
				<span>exit</span>
			</div>
		</div>
	);
};

export default SideBar;
