**cara menjalankan** 
1. clone 
2. cd **crud-automic-design**
3. **npm install**


**cara membuat promo**

1. register terlebih dahulu
2. selanjutnya login
3.  setelah login pilih menu promo
4. masukan promo

**automic design**

> Atomic Design adalah pendekatan desain yang dipopulerkan oleh Brad
> Frost, yang memecah elemen aplikasi web menjadi bagian-bagian modular
> hingga yang paling kecil.

![enter image description here](https://miro.medium.com/max/2048/1*0h8AvSjk0eQWr_liBcyA7g.png)

1. atom
![enter image description here](https://miro.medium.com/max/2048/1*RdbLlQS0RpmRQ3Bw5hJbxA.png)
Atom adalah bagian terkecil dari suatu materi. Pada atomic design atom ini berperan dalam perincian dan penyestrukturan dari desain UI tersbut. (Oh iya desain ui ini kita lihat desain UI yang umum. Tidak spesifik pada website atau mobile, namun jika pada illustrasinya seperti menjurus pada 1 bidang, itu hanya sebagai illustrasi saja, karena walau berbeda tetap satu jua). Oke back to the topic, atom pada desain ini berkaitan pada label (teks), shape, button, (mungkin juga hal yang lebih mendasar) warna.

2. MOLEKUL
![enter image description here](https://miro.medium.com/max/2048/1*OHeV_LO3mGilt7z60RHPgA.png)

molekul ini adalah gabungan dari atomic atomic kecil yang menjadi 1 kesatuan fungsi. Contohnya sebuah label dan sebuah input dan juga sebuah tombol.

3. ORGANISME
![enter image description here](https://miro.medium.com/max/2048/1*lfP46ddcd2kOjg4Ce8ZhgA.png)
Organisme disini adalah penggabungan dari atom dan molekul yang berbeda fungsi tapi masih pada petak yang sama. Misalkan pada navbar, disana umumnya ada beberapa label, 1 input, dan beberapa button. Meskipun mereka semua tidak 1 fungsi tapi mereka pada petak yang sama yaitu pada navbar.

4.  TEMPLATE
![enter image description here](https://miro.medium.com/max/2048/1*e8nZjPozJoPZ_NjgR6M1WQ.png)

Susunan-susunan dari organisme-organisme yang berada pada 1 halaman akan membentuk sebuah halaman utuh. Namun sebelum menjadi halaman yang utuh perlu adanya penataan organisme-organisme tadi pada sebuah layout. Pada layout ini kita tidak melihat bagaimana warna dari atom-atom tadi, bentuk rincinya seperti apa, maupun bagaimana ukuran pasti dari atom tadi. Tapi kita lebih ke arah letak dan posisi serta komposisi-komposisi dari atom, molekul, dan organisme tadi supaya membentuk sebuah halaman yang padu.

5.  PAGES
![enter image description here](https://miro.medium.com/max/2048/1*-BCQkJ587iBYwue-SNrTYw.png)

Page atau yang Bahasa Indonesianya adalah halaman, disini memiliki arti dan makna utama adalah apa yang menampung semua organisme-organisme tadi sehingga bisa ditampilkan dan menyajikan konten-konten dari pemilik web tersebut. Halaman disini lebih seperti pemantapan dari template yang sudah ada. Baik dari segi warna, ukuran, font, konten, dan gambar-gambar pendukung.